import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = '76f30f5433dd429c92f345dde3aa0503'
    app.config['SQLALCHEMY_DATABASE_URI'] = \
        os.environ['SQLALCHEMY_DATABASE_URI']
    app.config['UPLOAD_FOLDER'] = os.environ['UPLOAD_FOLDER']

    db.init_app(app)
    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    from .models import User
    with app.app_context():
        db.create_all()

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .files import files_bp
    app.register_blueprint(files_bp, url_prefix='/files')

    from .upload import upload as upload_blueprint
    app.register_blueprint(upload_blueprint)

    return app
