import os

from flask import Blueprint, redirect

from flask_autoindex import AutoIndexBlueprint
from flask_login import current_user

files_bp = Blueprint('files_bp', __name__)
AutoIndexBlueprint(files_bp, browse_root=os.environ['UPLOAD_FOLDER'])


@files_bp.before_request
def restrict_bp_to_authorized():
    if not current_user.is_authenticated:
        return redirect('/error')
