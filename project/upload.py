import os

from flask import Blueprint, request, render_template, current_app
from flask_login import login_required, current_user
from werkzeug.utils import secure_filename

upload = Blueprint('upload', __name__)


@upload.route('/upload')
@login_required
def upload_file_page():
    return render_template('upload.html', name=current_user.name)


@upload.route('/uploader', methods=['GET', 'POST'])
@login_required
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        f.save(os.path.join(current_app.config['UPLOAD_FOLDER'],
                            secure_filename(f.filename)))
        return 'file uploaded successfully'
