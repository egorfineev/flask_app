FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python3-pip python3-dev build-essential
WORKDIR /app
COPY requirements.txt requirements.txt
COPY . .
RUN pip3 install -r requirements.txt
ENV FLASK_APP=project
ENV SQLALCHEMY_DATABASE_URI=sqlite:///db.sqlite 
ENV UPLOAD_FOLDER=/app
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
